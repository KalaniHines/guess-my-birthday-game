from random import randint

name_prompt = input('Hi! What is your name?').lower()
guess_one = 1
month = randint(1, 12)
year = randint(1924, 2004)

print(f'Guess {guess_one} were you born in {month} {year}')

if guess_one == 'no':
    print('Drat! Lemme try again!')
